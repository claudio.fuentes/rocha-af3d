﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimiento : MonoBehaviour
{
    public float movimientoHorizontal;
    public float movimientoVertical;
    public CharacterController jugador;
    public float velocidad;
    public float velocidadRotacion;
    void Start()
    {
        jugador = GetComponent<CharacterController>();
        
    }

    // Update is called once per frame
    void Update()
    {
        movimientoHorizontal = Input.GetAxis("Horizontal");
        movimientoVertical = Input.GetAxis("Vertical");

        Traslacion();
        //Rotacion();
    }

    void Traslacion()
    {
        jugador.Move(new Vector3(movimientoHorizontal, 0, movimientoVertical) * velocidad * Time.deltaTime);
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            velocidad = velocidad * 10;
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            velocidad = velocidad / 10;
        }
    }
    void Rotacion()
    {
        jugador.transform.Rotate(new Vector3(0, Input.GetAxis("Mouse X")) * Time.deltaTime * velocidadRotacion);
        //GetComponent<Transform>().Rotate();
    }
}
